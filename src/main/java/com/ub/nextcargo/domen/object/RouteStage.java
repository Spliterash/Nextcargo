package com.ub.nextcargo.domen.object;

import com.ub.nextcargo.domen.entity.Voyage;

import java.time.LocalDateTime;

public class RouteStage {
    private Voyage voyage;
    private Location loadLocation, unloadLocation;
    private LocalDateTime loadTime;
    private LocalDateTime unloadTime;


    public Voyage getVoyage() {
        return voyage;
    }

    public Location getLoadLocation() {
        return loadLocation;
    }

    public Location getUnloadLocation() {
        return unloadLocation;
    }

    public LocalDateTime getLoadTime() {
        return loadTime;
    }

    public LocalDateTime getUnloadTime() {
        return unloadTime;
    }
}
