package com.ub.nextcargo.domen.object;

import com.ub.nextcargo.domen.entity.Voyage;
import com.ub.nextcargo.domen.object.enums.RoutingStatus;
import com.ub.nextcargo.domen.object.enums.TransportationStatus;

import java.time.LocalDateTime;

public class Delivery {
    private Voyage currentVoyage;
    private Location lastKnownLocation;
    private TransportationStatus transportationStatus;
    private RoutingStatus routingStatus;
    private LocalDateTime calculatedAt;

    public Delivery() {
    }

    public Delivery(Itinerary itinerary, Location origin, Location destination) {
        routingStatus = RoutingStatus.START;
        transportationStatus = TransportationStatus.START;
        calculatedAt = LocalDateTime.now();

    }

    public Voyage getCurrentVoyage() {
        return currentVoyage;
    }

    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    public TransportationStatus getTransportationStatus() {
        return transportationStatus;
    }

    public RoutingStatus getRoutingStatus() {
        return routingStatus;
    }

    public LocalDateTime getCalculatedAt() {
        return calculatedAt;
    }

    public void setTransportationStatus(TransportationStatus transportationStatus) {
        this.transportationStatus = transportationStatus;
    }

    public void setRoutingStatus(RoutingStatus routingStatus) {
        this.routingStatus = routingStatus;
    }
}
