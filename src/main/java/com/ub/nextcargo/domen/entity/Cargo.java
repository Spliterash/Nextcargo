package com.ub.nextcargo.domen.entity;


import com.ub.nextcargo.domen.object.Delivery;
import com.ub.nextcargo.domen.object.Itinerary;
import com.ub.nextcargo.domen.object.Location;
import com.ub.nextcargo.domen.object.enums.RoutingStatus;
import com.ub.nextcargo.domen.object.enums.TransportationStatus;

import java.time.LocalDate;
import java.util.UUID;

public class Cargo {
    private UUID id;
    private Customer customer;
    private Location origin, destination;
    private LocalDate originDate, destinationDate;
    private double weight;
    private Itinerary itinerary;
    private Delivery delivery;

    public Cargo() {
    }

    public void setDestination(Location location) {
        this.destination = location;
    }

    public Cargo(Location origin,
                 Location destination,
                 LocalDate originDate,
                 LocalDate destinationDate,
                 double weight) {
        setRoute(origin, destination, originDate, destinationDate);
        if (weight <= 0)
            throw new IllegalArgumentException("Weight must be bigger 0");
        this.weight = weight;
    }

    public void setRoute(Location origin,
                         Location destination,
                         LocalDate originDate,
                         LocalDate destinationDate) {
        if (origin == null) {
            throw new IllegalArgumentException("Origin can't be null");
        }
        if (destination == null) {
            throw new IllegalArgumentException("Destination can't be null");
        }
        if (origin.equals(destination)) {
            throw new IllegalArgumentException("Origin and destination don't be equals");

        } else {
            this.origin = origin;
            this.destination = destination;
        }
        if (originDate == null)
            throw new IllegalArgumentException("OriginDate can't be null");
        this.originDate = originDate;
        if (destinationDate == null)
            throw new IllegalArgumentException("Destination date can't be null");
        this.destinationDate = destinationDate;
        this.delivery = new Delivery(new Itinerary(), origin, destination);
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
        delivery.setRoutingStatus(RoutingStatus.START);
        delivery.setTransportationStatus(TransportationStatus.START);
    }
}
