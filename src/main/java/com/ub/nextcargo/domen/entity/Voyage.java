package com.ub.nextcargo.domen.entity;

import com.ub.nextcargo.domen.object.Schedule;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Voyage {
    @Id
    @GeneratedValue
    private UUID id;
    private String number;
    private Schedule schedule;
}
