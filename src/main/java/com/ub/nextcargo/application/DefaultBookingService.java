package com.ub.nextcargo.application;

import com.ub.nextcargo.domen.entity.Cargo;
import com.ub.nextcargo.domen.object.Itinerary;
import com.ub.nextcargo.domen.object.Location;

import java.time.LocalDate;

public class DefaultBookingService implements CargoBookingService {
    @Override
    public Cargo bookingCargo(Location origin,
                              Location destination,
                              LocalDate originDate,
                              LocalDate destinationDate,
                              double weight) {
        return new Cargo(origin, destination, originDate, destinationDate, weight);
    }

    @Override
    public void attachCargo(Cargo cargo, Itinerary itinerary) {
        cargo.setItinerary(itinerary);
    }

    @Override
    public void changeDestination(Cargo cargo, Location newDestination) {
        cargo.setDestination(newDestination);
    }
}
