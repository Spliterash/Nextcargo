package com.ub.nextcargo.application;

public class BookingServiceFactory {
    private static CargoBookingService service = new DefaultBookingService();

    public static CargoBookingService getService() {
        return service;
    }
}
