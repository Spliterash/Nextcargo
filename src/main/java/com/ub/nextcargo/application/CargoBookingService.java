package com.ub.nextcargo.application;

import com.ub.nextcargo.domen.entity.Cargo;
import com.ub.nextcargo.domen.object.Itinerary;
import com.ub.nextcargo.domen.object.Location;

import java.time.LocalDate;

public interface CargoBookingService {
    Cargo bookingCargo(Location origin,
                       Location destination,
                       LocalDate originDate,
                       LocalDate destinationDate,
                       double weight);

    void attachCargo(Cargo cargo, Itinerary itinerary);

    void changeDestination(Cargo cargo, Location newDestination);
}
