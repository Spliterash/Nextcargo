package com.ub.nextcargo;

import com.ub.nextcargo.application.BookingServiceFactory;
import com.ub.nextcargo.application.CargoBookingService;
import com.ub.nextcargo.domen.entity.Cargo;
import com.ub.nextcargo.domen.object.Itinerary;
import com.ub.nextcargo.domen.object.Location;
import org.apache.tomcat.jni.Local;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NextcargoApplicationTests {

    @Test
    public void testCargo() {
        Cargo cargo = new Cargo(
            new Location(0, 0, 0),
            new Location(5, 5, 5),
            LocalDate.now(),
            LocalDate.now().plusDays(7),
            10
        );
        cargo.setItinerary(new Itinerary());
        cargo.setRoute(
            new Location(1, 1, 1),
            new Location(10, 10, 10),
            LocalDate.now().plusDays(2),
            LocalDate.now().plusDays(10));
    }

    @Test
    public void testBookingService() {
        CargoBookingService service = BookingServiceFactory.getService();
        Cargo cargo = service.bookingCargo(
            new Location(0, 0, 0),
            new Location(5, 5, 5),
            LocalDate.now(),
            LocalDate.now().plusDays(7),
            10
        );
        service.attachCargo(cargo, new Itinerary());
        service.changeDestination(cargo, new Location(15, 15, 15));
    }

}
